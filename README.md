# irc

The incremental risk charge (IRC) is a regulatory requirement from the Basel Committee in response to the financial crisis. It supplements existing Value-at-Risk (VaR) and captures the loss due to default and migration events at a 99.9% confidence le